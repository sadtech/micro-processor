package ru.rsreu.oop.microprocessor.core.service;

import org.springframework.stereotype.Service;
import ru.rsreu.oop.microprocessor.core.domain.Status;

@Service
public class StatusServiceImpl implements StatusService {

    private final Status status = new Status();

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void checkResult(long result) {
        if (result == 0) {
            status.setZ(1);
        } else {
            status.setZ(0);
        }

        if (result >= 0) {
            status.setN(0);
        } else {
            status.setN(1);
        }

        if (result > Short.MAX_VALUE - 1) {
            status.setV(1);
        } else {
            status.setV(0);
        }

        if (result % 2 == 0) {
            status.setP(1);
        } else {
            status.setP(0);
        }

    }
}
