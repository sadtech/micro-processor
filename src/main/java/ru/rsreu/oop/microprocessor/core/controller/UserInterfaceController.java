package ru.rsreu.oop.microprocessor.core.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rsreu.oop.microprocessor.core.domain.Status;
import ru.rsreu.oop.microprocessor.core.operation.*;
import ru.rsreu.oop.microprocessor.core.service.StatusService;
import ru.rsreu.oop.microprocessor.core.controller.view.EditingCell;
import ru.rsreu.oop.microprocessor.core.controller.view.View;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class UserInterfaceController {

    private static final int COUNT_REGISTERS = 16;

    @FXML
    private ChoiceBox commandChoice;
    @FXML
    private ChoiceBox firstOperand;
    @FXML
    private ChoiceBox secondOperand;
    @FXML
    public TableView<View> registers;
    @FXML
    private TableView<Status> regStatus;
    @FXML
    private TableColumn regName;
    @FXML
    private TableColumn regValue;
    @FXML
    private TableColumn flagN;
    @FXML
    private TableColumn flagP;
    @FXML
    private TableColumn flagV;
    @FXML
    private TableColumn flagZ;

    private static final String[] COMMANDS = {"ADD", "SUB", "AND", "OR", "XOR"};
    private final ObservableList<String> register = createOperand();
    private ObservableList<View> regViews = FXCollections.observableArrayList();

    @Autowired
    private StatusService statusService;

    public void loadElement() {
        commandChoice.setItems(FXCollections.observableList(Arrays.asList(COMMANDS)));
        commandChoice.setValue(COMMANDS[0]);

        firstOperand.setItems(register);
        firstOperand.setValue(register.get(0));

        secondOperand.setItems(register);
        secondOperand.setValue(register.get(1));

        initRegisters();

        Callback<TableColumn, TableCell> cellFactory = p -> new EditingCell();
        regName.setCellValueFactory(new PropertyValueFactory<View, String>("name"));
        regValue.setCellValueFactory(new PropertyValueFactory<View, String>("value"));
        regValue.setCellFactory(cellFactory);
        regValue.setOnEditCommit((EventHandler<TableColumn.CellEditEvent<View, String>>) event -> {
            StringBuilder stringBuilder = new StringBuilder(event.getNewValue());
            stringBuilder = stringBuilder.deleteCharAt(0).deleteCharAt(0);
            event.getTableView().getItems()
                    .get(event.getTablePosition().getRow())
                    .setValue((short) (Integer.parseInt(stringBuilder.toString(), 16)));
            event.getTableView().refresh();
        });
        flagN.setCellValueFactory(new PropertyValueFactory<Status, String>("n"));
        flagZ.setCellValueFactory(new PropertyValueFactory<Status, String>("z"));
        flagV.setCellValueFactory(new PropertyValueFactory<Status, String>("v"));
        flagP.setCellValueFactory(new PropertyValueFactory<Status, String>("p"));
        registers.setItems(regViews);
        registers.refresh();
        regStatus.setItems(FXCollections.observableList(Collections.singletonList(statusService.getStatus())));
        regStatus.refresh();
    }

    private ObservableList<String> createOperand() {
        List<String> lines = new ArrayList<>();
        for (int i = 0; i < COUNT_REGISTERS; i++) {
            lines.add("R" + i);
        }
        return FXCollections.observableArrayList(lines);
    }

    private void initRegisters() {
        for (int i = 0; i < COUNT_REGISTERS; i++) {
            regViews.add(new View(register.get(i), (short) 0));
        }
    }

    public void executeCommands() {
        int num1 = firstOperand.getSelectionModel().getSelectedIndex();
        int num2 = secondOperand.getSelectionModel().getSelectedIndex();
        short reg1 = regViews.get(num1).getShortValue();
        short reg2 = regViews.get(num2).getShortValue();
        Command command = getCommand(commandChoice.getSelectionModel().getSelectedIndex());
        if (command != null) {
            long result = command.executeCommand(reg1, reg2);
            statusService.checkResult(result);
            regViews.get(secondOperand.getSelectionModel().getSelectedIndex()).setValue((short) result);
            registers.setItems(regViews);
            registers.refresh();
            regStatus.setItems(FXCollections.observableList(Collections.singletonList(statusService.getStatus())));
            regStatus.refresh();
        }
    }

    private Command getCommand(Integer integer) {
        switch (integer) {
            case 0: {
                return new ADD();
            }
            case 1: {
                return new SUB();
            }

            case 2: {
                return new AND();
            }

            case 3: {
                return new OR();
            }

            case 4: {
                return new XOR();
            }
            default: {
                return null;
            }
        }
    }

}
