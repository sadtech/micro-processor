package ru.rsreu.oop.microprocessor.core.service;

import ru.rsreu.oop.microprocessor.core.domain.Status;

public interface StatusService {

    Status getStatus();

    void checkResult(long result);

}
