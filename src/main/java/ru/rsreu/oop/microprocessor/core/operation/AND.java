package ru.rsreu.oop.microprocessor.core.operation;

public class AND implements Command {

    @Override
    public short executeCommand(short register1, short register2) {
        return (short) (register1&register2);
    }

}
