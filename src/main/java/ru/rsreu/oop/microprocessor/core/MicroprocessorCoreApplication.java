package ru.rsreu.oop.microprocessor.core;

import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.rsreu.oop.microprocessor.core.config.ConfigurationControllers;
import ru.rsreu.oop.microprocessor.core.controller.UserInterfaceController;

@SpringBootApplication
public class MicroprocessorCoreApplication extends AbstractJavaFxApplicationSupport {

    @Value("${ui.title:Графический редактор}")//
    private String windowTitle;

    @Autowired
    private ConfigurationControllers.View view;

    @Override
    public void start(Stage stage) {
        stage.setTitle(windowTitle);
        stage.setScene(new Scene(view.getParent()));
        stage.setResizable(true);
        stage.centerOnScreen();
        stage.setOnShown(e -> ((UserInterfaceController) view.getController()).loadElement());
        stage.show();
    }

    public static void main(String[] args) {
        launchApp(MicroprocessorCoreApplication.class, args);
    }

}
