package ru.rsreu.oop.microprocessor.core.controller.view;


import javafx.scene.control.TextField;

public class SpecTextField extends TextField {

    private static final String LINE = "1234567890xabcdef.\b";

    public SpecTextField(String string) {
        super(string);
    }

    @Override
    public void replaceText(int start, int end, String text) {
        if (!LINE.contains(text))
            text = "";
        else {
            if (!this.getText().contains("0x"))
                text = "0x" + this.getText() + text;
        }
        super.replaceText(start, end, text);
    }
}
