package ru.rsreu.oop.microprocessor.core.domain;

public class Status {
    private int n;
    private int z;
    private int v;
    private int p;

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    public int getP() {
        return p;
    }

    public void setP(int p) {
        this.p = p;
    }
}
