package ru.rsreu.oop.microprocessor.core.operation;

public interface Command {

    short executeCommand(short register1, short register2);

}
