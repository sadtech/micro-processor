package ru.rsreu.oop.microprocessor.core.controller.view;

public class View {
    private String name;
    private String value;

    public View(String name, short value) {
        this.name = name;
        setValue(value);
    }

    public short getShortValue() {
        StringBuilder line = new StringBuilder(value);
        line = line.deleteCharAt(0).deleteCharAt(0);
        return (short) Integer.parseUnsignedInt(line.toString(), 16);
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public void setValue(short value) {
        StringBuilder line = new StringBuilder("0x");
        line.append(Integer.toHexString(value));
        this.value = line.toString();
    }

    @Override
    public String toString() {
        StringBuilder line = new StringBuilder("View[name=");
        line.append(name).append(";value=").append(value).append(']');
        return line.toString();
    }

}
